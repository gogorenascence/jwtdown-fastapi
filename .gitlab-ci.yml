# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
stages:
- test
- build
- deploy

variables:
  SECURE_LOG_LEVEL: error
  POETRY_CACHE_DIR: "${CI_PROJECT_DIR}/.cache/poetry"
  POETRY_HOME: "${CI_PROJECT_DIR}/.poetry"

cache: &global_cache
  key:
    files:
      - pyproject.toml
  paths:
    - ${POETRY_CACHE_DIR}
    - ${CI_PROJECT_DIR}/.venv
    - ${POETRY_HOME}

.common_python_setup: &common_python_setup |
  if [[ ! -x "${POETRY_HOME}/bin/poetry" ]]
  then
    apt-get install -y -qq curl
    curl -sSL https://install.python-poetry.org | python -
    export PATH="${POETRY_HOME}/bin:$PATH"
    poetry config virtualenvs.in-project true
    poetry install
  else
    export PATH="${POETRY_HOME}/bin:$PATH"
    poetry self update
  fi
  echo "Using poetry $(poetry --version)"

include: ".tox-ci.yml"

build-library:
  cache:
    <<: *global_cache
  stage: build
  image: python:3.11-bullseye
  needs:
    - tests-done
  before_script:
    - *common_python_setup
  script:
    - poetry build
  rules:
    - if: $CI_DEFAULT_BRANCH == $CI_COMMIT_BRANCH


build-library-from-tag:
  cache:
    <<: *global_cache
  stage: build
  image: python:3.11-bullseye
  needs:
    - tests-done
  before_script:
    - *common_python_setup
  script:
    - echo "Building for ${CI_COMMIT_TAG}"
    - poetry build
  artifacts:
    expire_in: never
    paths:
      - ./dist
  rules:
    - if: $CI_COMMIT_TAG


deploy-pypi:
  cache:
    <<: *global_cache
  stage: deploy
  image: python:3.11-bullseye
  needs:
    - build-library-from-tag
  dependencies:
    - build-library-from-tag
  before_script:
    - *common_python_setup
  script:
    - echo "Building for ${CI_COMMIT_TAG}"
    - echo "Using token ${PYPI_TOKEN}"
    - poetry config pypi-token.pypi ${PYPI_TOKEN}
    - poetry publish
  rules:
    - if: $CI_COMMIT_TAG
