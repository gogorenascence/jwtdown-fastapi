import httpx
import re
from semver import VersionInfo


PACKAGE_URL = "https://pypi.org/pypi/fastapi/json"
MINIMUM_VERSION = VersionInfo.parse("0.89.0")
PYTHON_VERSIONS = ["39", "310", "311"]

PREAMBLE = """
cache: &global_cache
  key:
    files:
      - pyproject.toml
  paths:
    - ${POETRY_CACHE_DIR}
    - ${CI_PROJECT_DIR}/.venv
    - ${POETRY_HOME}

.common_python_setup: &common_python_setup |
  if [[ ! -x "${POETRY_HOME}/bin/poetry" ]]
  then
    apt-get install -y -qq curl
    curl -sSL https://install.python-poetry.org | python -
    export PATH="${POETRY_HOME}/bin:$PATH"
    poetry config virtualenvs.in-project true
    poetry install
  else
    export PATH="${POETRY_HOME}/bin:$PATH"
    poetry self update
  fi
  echo "Using poetry $(poetry --version)"

.test_versions: &test_versions
  cache:
    <<: *global_cache                           # Defined in .gitlab-ci.yml
    policy: pull
  stage: test
  image: python:3.11-bullseye
  before_script:
    - *common_python_setup                      # Defined in .gitlab-ci.yml
  script:
    - poetry run tox -e ${TEST_ENVIRONMENT}
  rules:
    - if: $CI_COMMIT_BRANCH
    - if: $CI_COMMIT_TAG

tox-setup:
  cache:
    <<: *global_cache                           # Defined in .gitlab-ci.yml
  stage: test
  image: python:3.11-bullseye
  before_script:
    - *common_python_setup                      # Defined in .gitlab-ci.yml
  script:
    - echo "Common Python environment set up."
  rules:
    - if: $CI_COMMIT_BRANCH
    - if: $CI_COMMIT_TAG
"""

# Positional formatting placeholders:
#   1. Python version
#   2. FastAPI version
TEMPLATE = """
tox-{0}-{1}:
  needs:
    - tox-setup
  variables:
    TEST_ENVIRONMENT: python{0}-fastapi{1}
  <<: *test_versions
"""

# Positional formatting placeholders
#   1. The YAML list of tox jobs, four-spaced indented
POSTSCRIPT = """
tests-done:
  needs:
{}
  image: alpine:latest
  script:
    - echo "Tests done"
"""

# Positional format parameters
#   1. Python version numbers as comma-delimited list
#   2. FastAPI version numbers as comma-delimited list
#   3. Environment dependency list with four spaces before each line
PYPROJECT_SECTION = """
[tool.tox]
legacy_tox_ini = \"\"\"
[tox]
envlist = python{{{0}}}-fastapi{{{1}}}
skipsdist = true
requires = tox-poetry-installer[poetry]

[testenv]
deps =
{2}
	pytest>=7.1.3
description = A testing environment
install_project_deps = True
poetry_dep_groups = [dev]
require_poetry = True
commands = pytest
\"\"\"
"""


if __name__ == "__main__":
    response = httpx.get(PACKAGE_URL)
    response.raise_for_status()
    package_description = response.json()
    release_versions: list[str] = package_description["releases"].keys()
    good_versions = list(
        sorted(
            [
                version
                for version in release_versions
                if VersionInfo.parse(version) >= MINIMUM_VERSION
            ]
        )
    )
    with open(".tox-ci.yml", "w") as fp:
        fp.write(PREAMBLE)
        jobs = []
        for python_version in PYTHON_VERSIONS:
            for fastapi_version in good_versions:
                fastapi_version = fastapi_version.replace(".", "")
                fp.write(TEMPLATE.format(python_version, fastapi_version))
                jobs.append(
                    "    - tox-{}-{}".format(python_version, fastapi_version)
                )
        fp.write(POSTSCRIPT.format("\n".join(jobs)))
    with open("pyproject.toml") as fp:
        content = fp.read()
        truncate_index = content.find("[tool.tox]")
    with open("pyproject.toml", "w") as fp:
        truncated = content[:truncate_index].strip()
        truncated = re.sub(
            r"fastapi\s+=\s+\".*",
            f'fastapi = ">={MINIMUM_VERSION},<={good_versions[-1]}"',
            truncated,
        )
        truncated = re.sub(
            r"fastapi\s+=\s+{version.*",
            f'fastapi = {{version = "^{MINIMUM_VERSION}", extras = ["all"]}}',
            truncated,
        )
        fp.write(truncated)
        fp.write("\n")
        python_version_list = ",".join(PYTHON_VERSIONS)
        stripped_fastapi_versions = [
            version.replace(".", "") for version in good_versions
        ]
        fastapi_version_list = ",".join(stripped_fastapi_versions)
        deps = [
            "    python{{{0}}}-fastapi{1}: fastapi=={2}".format(
                python_version_list,
                stripped,
                canonical,
            )
            for stripped, canonical in zip(
                stripped_fastapi_versions,
                good_versions,
            )
        ]
        fp.write(
            PYPROJECT_SECTION.format(
                python_version_list,
                ",".join(stripped_fastapi_versions),
                "\n".join(deps),
            )
        )
